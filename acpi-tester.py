#!/usr/bin/python3

"""Simple script to test ACPI in the Redox OS kernel"""

import tempfile
import shutil
import subprocess
from pathlib import Path
import sys

source_path_to_redox=sys.argv[1]

temp_path = tempfile.mkdtemp()

try:
    subprocess.run(["ls -R"], shell=True, cwd=temp_path)
    temp_src_path = Path(temp_path, "src")
    temp_src_path.mkdir()

    temp_src_path.joinpath("lib.rs").write_text("""#![no_std]
#![feature(alloc)]
#![feature(core_intrinsics)]

#[macro_use]
extern crate alloc;

extern crate spin;

extern crate syscall;

#[cfg(test)]
#[macro_use]
extern crate std;

#[cfg(not(test))]
macro_rules! println {
    ($fmt:expr) => ();
    ($fmt:expr, $($arg:tt)*) => ();
}

#[cfg(not(test))]
macro_rules! print {
    ($fmt:expr) => ();
    ($fmt:expr, $($arg:tt)*) => ();
}

pub mod acpi;
""")

    temp_acpi_path = temp_src_path.joinpath("acpi")
    shutil.copytree(Path(source_path_to_redox, "src/acpi"), temp_acpi_path)

    path_to_syscall = Path(source_path_to_redox, "syscall")

    Path(temp_path, "Cargo.toml").write_text("""[package]
name = "acpi_tester"
version = "0.0.1"

[lib]
name = "acpi"
path = "src/lib.rs"
test = true

[dependencies]
spin = "0.4.8"
redox_syscall = {{ path = "{}" }}

[features]
default = ["core_intrinsics", "multicore"]
core_intrinsics = []
multicore = []
""".format(path_to_syscall))

    subprocess.run(["ls -R"], shell=True, cwd=temp_path)
    subprocess.run(["cargo test -- --nocapture  --test-threads=1"], shell=True, cwd=temp_path)

except Exception as e:
    print (e)
finally:
    shutil.rmtree(temp_path)
