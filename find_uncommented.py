#! /usr/bin/python3
"""Script to find uncommented functions, structs, ... in a Rust codebase

Usage: python3 find_uncommented.py [path]"""

import os
import sys
import re

def main(path):
    for root, dirs, files in os.walk(path):
        explore(root)
        for d in dirs:
            explore(os.path.join(root, d))

def display(l, commented):
    if commented or l.startswith("//"):
        # print ("COMMENTED: "+l)
        pass
    else:
        print (l)

def explore(d):
    print ("***", d)
    for root, dirs, files in os.walk(d):
        for fname in files:
            if not fname.endswith(".rs"):
                continue

            fullpath = os.path.join(root, fname)
            explore_file(fullpath)

def explore_file(fullpath):
    print (">>>", fullpath)
    commented = False
    with open(fullpath, 'r', encoding="utf-8") as f:
        for line in f.readlines():
            l = line.strip()
            if re.search("^(pub\s+)?(fn|struct|trait|enum|type|const)\s+\w+", l):
                display(l, commented)

            if l.startswith("///"):
                commented = True
            elif not (l.startswith("#[") or l.startswith("//")):
                commented = False


if len(sys.argv) == 1:
    main (os.getcwd())
elif len(sys.argv) == 2:
    main (sys.argv[1])
else:
    print("""Usage: python3 find_uncommented.py [path]""")
